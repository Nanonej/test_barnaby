require 'test_helper'

class PlacesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @access_token = ENV.fetch('ACCESS_TOKEN')
  end

  test "should get unauthorized code" do
    get places_search_url
    assert_response 401
    get places_search_url,
      headers: { "Authorization": "test", "Content-Type": "application/json" }
    assert_response 401
  end

  test "should get invalid request" do
    get places_search_url,
      headers: { "Authorization": @access_token, "Content-Type": "application/json" }
    assert_response 400
  end

  test "should get success" do
    get places_search_url,
      headers: { "Authorization": @access_token, "Content-Type": "application/json" },
      params: { q: "test" }
    assert_response :success
  end

end

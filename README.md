# README

## Getting started

To get started with the app, clone the repo and then install the right ruby version

```
$ rbenv install
```

or check you have the same version as the .ruby-version with

```
$ ruby -v
```

Then install the needed gems:

```
$ bundle install --without production
```

Finally, run the test suite to verify that everything is working correctly:

```
$ bin/rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ bin/rails server
```

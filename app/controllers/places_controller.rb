class PlacesController < ApplicationController
  before_action :authenticate_request
  before_action :validate_request

  G_API_URL = 'https://maps.googleapis.com/maps/api/place/autocomplete/json'

  def extract
    @extract ||= JSON.load File.read(Rails.root.join('db/extract.json'))
  end

  def auto_id
    @auto_id ||= JSON.load File.read(Rails.root.join('db/auto_id.json'))
  end

  def search
    search_str = params[:q]

    response = RestClient.get G_API_URL,
      params: {
        input: search_str,
        types: 'establishment',
        components: 'country:fr',
        key: ENV.fetch('G_API_KEY')
      }
    google_json = JSON.parse(response.body)

    extract_pl = extract["places"].select do |place|
      place["name"].include? search_str
    end

    google_pl = []
    if google_json["status"] === "OK"
      google_json["predictions"].each do |place|
        entry = auto_id.find { |entry| entry["g_id"] == place["id"] }
        google_pl << entry if entry
      end
    end

    render json: {
      success: true,
      payload: {
        extract: extract_pl,
        google: google_pl
      }
    }, status: 200
  end

  def render_error(message, status)
    render json: { success: false, error: message }, status: status
  end

  private

    def authenticate_request
      header_token = request.headers["Authorization"]
      access_token = ENV.fetch('ACCESS_TOKEN')
      render_error 'Not Authorized', 401 unless access_token == header_token
    end

    def validate_request
      render_error 'Invalid Request', 400 if params[:q].nil?
    end

end
